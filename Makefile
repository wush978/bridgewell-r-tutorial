all: Chapter0.html

%.html: %.Rmd
	Rscript -e "library(slidify);slidify('$<')"

clean:
	rm *.html
