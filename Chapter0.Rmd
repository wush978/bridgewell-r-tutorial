---
title       : Bridgewell R Tutorial
subtitle    : Getting Start
author      : Wush Wu
job         : Taiwan R User Group
license: by-nc-sa
logo: Taiwan-R-logo.png
framework   : io2012        # {io2012, html5slides, shower, dzslides, ...}
highlighter : highlight.js  # {highlight.js, prettify, highlight}
hitheme     : zenburn       # 
widgets     : []            # {mathjax, quiz, bootstrap}
mode        : selfcontained # {standalone, draft}
---

```{r setup, include=FALSE,echo = F, message = F, warning = F, tidy = F}
# make this an external chunk that can be included in any file
library(xtable)
library(reshape2)
library(knitr)
options(width = 100,digits=3)
opts_chunk$set(message = FALSE, eval=TRUE,fig.align = "center", warning = FALSE, comment = NA, dpi = 100, fig.width=6, fig.height=4.5,tidy = FALSE, cache = FALSE, echo=FALSE)

options(xtable.type = 'html')
knit_hooks$set(inline = function(x) {
  if(is.numeric(x)) {
    round(x, getOption('digits'))
  } else {
    paste(as.character(x), collapse = ', ')
  }
})
knit_hooks$set(plot = knitr:::hook_plot_html)
```

## Outline

- R 是什麼？
- 有誰在用R？
- R 能做什麼？
- 怎麼開始玩R?
- 怎麼學R?

--- .segue .dark

## R 是什麼？

--- &twocol

## R 的資料

*** =left

1. 主要應用於資料分析的程式語言
    - 能做到大部份的工具能做的事情
    - Open Source: 使用者可以自行擴充R 的功能
    - 目前擁有5000多個套件
2. 有非常多方便的函數用於：
    - 資料收集
    - 資料整理
    - 建立統計模型
    - 視覺化

*** =right

<img src="assets/img/Rlogo.png" class="fit100"/>

--- &twocol

## R 的長相?

*** =left

傳統的R界面

<img src="assets/img/R_console.png" class="fit100" />

*** =right

[Tinn-R](http://www.sciviews.org/Tinn-R/)

<img src="assets/img/tinn_r.png" class="fit100" />

---

## R 的長相?

<img src="assets/img/rstudio.gif" class="fit100" />

--- .segue .dark

## 有誰在使用R?

---

## Google:

> R is the most popular statistical package at Google(取自[How Google and Facebook are using R](http://www.dataspora.com/2009/02/predictive-analytics-using-r/))

根據小道消息，Google甚至有邀請顧問公司對Google的員工進行R 的教育訓練。

<img src="assets/img/Googlelogo.png" class="fit100"/>

---

## [googleVis](https://code.google.com/p/google-motion-charts-with-r/)

<iframe src='http://rstudio-pubs-static.s3.amazonaws.com/11932_50ff228a079c4c4b9ac558a81e1bf66a.html'></iframe>

---

## Facebook

> Facebook’s Data Team used R in 2007 to answer two questions about new users: (i) which data points predict whether a user will stay? and (ii) if they stay, which data points predict how active they’ll be after three months?(取自[How Google and Facebook are using R](http://www.dataspora.com/2009/02/predictive-analytics-using-r/)

<img src="assets/img/facebook.jpg" class="fit100"/>

---

## [Rfacebook](http://cran.r-project.org/web/packages/Rfacebook/)

---

## Kaggle

Kaggle是全世界最火熱的資料分析競賽平台。Kaggle的參賽者使用的分析工具：

<div style='text-align: center;'>
    <img src="assets/img/kaggle.png" class="fit100" />
</div>

(取自[How Kaggle competitors use R](http://blog.revolutionanalytics.com/2011/04/how-kaggle-competitors-use-r.html))

--- .segue .dark

## R 能做什麼？

--- &twocol

## 資料整理

取自<http://www.meetup.com/Taiwan-R/events/161344072/>

`library(reshape2)`

*** =left

<div style="text-align: center;">
<code>melt-></code>
</div></br>

```{r, results='asis', echo=FALSE}
names(airquality) <- tolower(names(airquality))
print(xtable(head(airquality)), type="html")
```

*** =right

<div style="text-align: center;">
<code></code>
</div></br>

```{r, results='asis', echo=FALSE}
print(xtable(head(melt(airquality, id=c("month", "day")))), type="html")
```


---

## 統計分析 <http://glimmer.rstudio.com/wush978/LOLChampion/>

<div style='text-align: center;'>
    <img src="assets/img/lol.png" class="fit100" />
</div>

---

## 繪圖

取自<http://www.meetup.com/Taiwan-R/events/161344072/>

<img class="fit100" src="assets/img/ggLiang-so-strong.png"/>

---

## 撰寫報告

<div style='text-align: center;'>
    <img src="assets/img/knitr.png" class="fit100" />
</div>


--- &twocol

## R 是個功能完善的Scripting Language

- Python, php, javascript, Ruby, ... 能做的，R 都能做
    - Regular Expression
    - Web Service
    - Crawler
    - System Call
    - 族繁不及備載

*** =left

### Shiny

*** =right

### SpideR

---

## R 易於擴充 [Writing R Extensions](http://cran.r-project.org/doc/manuals/R-exts.html)

- Fortran
- C API
- Rcpp
- rJava
...

能串接C 的工具都可以和R 串接。除了CRAN之外，[Omegahat](http://www.omegahat.org/)有一大堆這類專案：

---

## R 能進行高效能運算

[High-Performance and Parallel Computing with R](http://cran.r-project.org/web/views/HighPerformanceComputing.html)

- 各種平行化運算
    - 外部平行化
    - 內部平行化
    - GPU
    - Grid Computing
    - Hadoop
- 平行化的隨機變數產生器
- 應用
    - [caret]Cross Validation and Bootstrap

--- .segue .dark

## 怎麼開始玩R?

---

## 先裝R 就對了！

- [在Windows上安裝R](http://youtu.be/FsOHPGUIDZU)
    - [在Windows上安裝Rtools](http://youtu.be/enPPMHr5SrM)
    - [在Windows上安裝Rstudio](http://youtu.be/jInpdE11ib0)
    - [在Windows上安裝Notepad++和NppToR]()
    - [在Windows上安裝tinn-R]()
- [在Mac OS X上安裝R]()
    - [在Mac上安裝Rstudio]()
- [在Ubuntu上安裝R]()
    - [在Ubuntu上安裝Rstudio]()
    - [在Ubuntu上安裝Rstudio Server]()
    - [在Ubuntu上安裝Shiny Server]()

--- .segue .dark

## 怎麼學R ?

--- &twocol

## 線上學習資源

這是個線上學習資源無窮無盡的時代

*** =left

### 英文

- [codeschool, 線上互動式的R學習網站](http://tryr.codeschool.com/)
- [兩分鐘的R簡介](http://www.twotorials.com/)
- [電子書(Data Science Book)](http://jsresearch.net/groups/teachdatascience/wiki/welcome/attachments/fb750/DataScienceBookV2.pdf)
- [Google Dev的許多教學影片](http://www.youtube.com/user/GoogleDevelopers)
- [R and Data Mining](http://cran.r-project.org/doc/contrib/Zhao_R_and_data_mining.pdf)

*** =right

### 中文

- [R導論](http://www.biosino.org/pages/newhtm/r/tchtml/)
- [國網中心的R學習筆記](http://statlab.nchc.org.tw/rnotes/)
- [陳鐘誠老師的免費電子書 -- 機率與統計 (使用 R 軟體)](http://ccckmit.wikidot.com/st:main)
- [R學習地圖](https://www.lucidchart.com/documents/edit/4634-8920-52dbcaa7-afed-66400a009db8)

--- &twocol

## 有問題嗎？請到下列地方發問：

*** =left

### 英文線上資源：

- [R mailing list](https://stat.ethz.ch/mailman/listinfo/r-help)
- [RStudio Support](https://support.rstudio.com/hc/en-us)
- [Stackoverflow](http://stackoverflow.com/) 或 [SuperUser](http://superuser.com/)

*** =right

### 中文討論區：

- Taiwan R User Group
    - [Mailing List](http://www.meetup.com/Taiwan-R/messages/archive/)
    - [Facebook粉絲團](https://www.facebook.com/Tw.R.User)
    - MLDM Monday找Officiers
- [ptt R_Language](http://www.ptt.cc/bbs/R_Language/index.html)
- [台灣R使用者論壇](https://groups.google.com/forum/#!forum/taiwanruser-)
- [統計之都](http://cos.name/cn/forum/15)

---

## 基礎的學不夠，想要更深入嗎？

- [R Advanced](http://adv-r.had.co.nz/)

